export default {
    ENV: process.env.ENV || "local",
    DB_USERNAME: process.env.DB_USERNAME as string,
    DB_PORT: process.env.DB_PORT,
    DB_PASSWORD: process.env.DB_PASSWORD  as string,
    DB_NAME: process.env.DB_NAME  as string,
    DB_HOST: process.env.DB_HOST  as string,

    AWS_REGION: process.env.AWS_REGION || "local",
    AWS_DYNAMO_DB: process.env.AWS_DYNAMO_DB || "http://localhost:8000",
    AWS_ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID || "local",
    AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY || "local"
}
  
  