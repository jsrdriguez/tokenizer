import dotenv from "dotenv"
dotenv.config();
import dayjs from "dayjs";
import { DynamoDB } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient, ScanCommand, DeleteCommand } from "@aws-sdk/lib-dynamodb";
import config from "./config";

const clientDynamoDb = new DynamoDB({ 
    region: config.AWS_REGION,
    endpoint: config.AWS_DYNAMO_DB,
    credentials: {
        accessKeyId: config.AWS_ACCESS_KEY_ID,
        secretAccessKey: config.AWS_SECRET_ACCESS_KEY
    }
}); 

const clientDynamo = DynamoDBDocumentClient.from(clientDynamoDb);

(async () => {
    console.log("Ejecutado....", dayjs().format('YYYY-MM-DD HH:mm:ss'));
    
    const command = new ScanCommand({
        TableName: "tokens",
        AttributesToGet: ["token_id", "createdAt"]
    });
    
    const { Items } = await clientDynamo.send(command);
    if (Items?.length === 0) {
        return;
    }

    const nowDate = dayjs();
    for (let i in Items) {
        let { token_id, createdAt} = Items[parseInt(i)];
        let dateItem = dayjs.unix(createdAt)
        if (nowDate.diff(dateItem, 'minute') < 15) {
            continue;
        }

        const command = new DeleteCommand({
            TableName: "tokens",
            Key: {
              token_id: token_id,
            },
          });
        
        const response = await clientDynamo.send(command);
        
        console.log(response);
    }
})()

