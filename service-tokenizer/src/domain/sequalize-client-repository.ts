import { DataTypes, Model, ModelOptions, Sequelize } from 'sequelize';


class SequelizeClientRepository {
    private sequelizeClient:Sequelize;
    private tokenModel: any;

    constructor(sequelizeClient: Sequelize) {
        this.sequelizeClient = sequelizeClient;

        this.tokenModel = this.sequelizeClient.define('Client', {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
            },
            client_hash: {
                type: DataTypes.STRING
            },
            name: {
                type: DataTypes.STRING
            }
        },{
            tableName: "clients",
            timestamps: false
        });
    }

    async getClient(pk: string): Promise<any>{
        return await this.tokenModel.findOne({ where: { client_hash: pk } })
    }

}


export default SequelizeClientRepository;