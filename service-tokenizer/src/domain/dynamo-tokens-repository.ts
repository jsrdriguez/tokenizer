import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient, GetCommand, PutCommand } from "@aws-sdk/lib-dynamodb";
import { IToken } from "./interface-repository";


class DynamoTokensRepository {
    dynamoClient:DynamoDBClient;

    constructor(dynamoClient: DynamoDBClient) {
        this.dynamoClient = DynamoDBDocumentClient.from(dynamoClient);
    }

    async save(data: IToken): Promise<any>{
        const command = new PutCommand({
            TableName: "tokens",
            ConditionExpression: "attribute_not_exists(token_id)",
            Item: {
                "token_id": data.token,
                "card_number": data.card_number,
                "cvv": data.cvv,
                "expiration_month":data.expiration_month,
                "expiration_year": data.expiration_year,
                "email": data.email,
                "createdAt": data.createdAt
            }
        });

        await this.dynamoClient.send(command);

        return await this.getCard(data.token);
    }

    async getCard(token:string): Promise<any> {
        const command = new GetCommand({
            TableName: "tokens",
            AttributesToGet: ["token_id", "card_number", "expiration_month", "expiration_year", "email"],
            Key: {
                "token_id": token
            }
        });

        const { Item } = await this.dynamoClient.send(command);

        return Item
    }   
}


export default DynamoTokensRepository;