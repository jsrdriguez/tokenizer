import { IToken } from '../domain/interface-repository';
import DynamoTokensRepository from '../domain/dynamo-tokens-repository';
import SequelizeClientRepository from '../domain/sequalize-client-repository';


class TokenController {
    tokenRepository: DynamoTokensRepository;
    clientRepository: SequelizeClientRepository;

    constructor(tokenRepository: DynamoTokensRepository, clientRepository: SequelizeClientRepository) {
        this.tokenRepository = tokenRepository;
        this.clientRepository = clientRepository;
    }

    async createToken(data: IToken): Promise<any> {
        return await this.tokenRepository.save(data);    
    }

    async getToken(token: string): Promise<any> {
        return await this.tokenRepository.getCard(token);    
    }

    async getClient(pk: string): Promise<any> {
        if (!pk) return null;

        return await this.clientRepository.getClient(pk.split(" ")[1]);
    }
}

export default TokenController