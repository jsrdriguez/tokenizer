import dayjs from "dayjs";
import { Router, Request, Response } from "express";
import ShortUniqueId from 'short-unique-id';
import TokenController from "./token.controller";
import { wrapBodyValidateSchema } from "./utils";
import { BodyData, headerSchema, paramsSchema, tokenSchema } from "./dto";


export default function TokenRouter(controller: TokenController) {
    const api = Router();

    api.post('/tokens', 
        wrapBodyValidateSchema(headerSchema, "header"), 
        wrapBodyValidateSchema(tokenSchema), 
        async (req: Request, res: Response) => {
            try {
                const { randomUUID } = new ShortUniqueId({ length: 16 });
                const createdAt = dayjs().format('YYYY-MM-DD HH:mm:ss');
                const body:BodyData = req.body;
                const autorization = req.headers.authorization as string;
                const client = await controller.getClient(autorization);

                if (client === null) {
                    return res.status(401).send({
                        "error": true,
                        "message": "Unauthorized"
                    });
                }

                const result = await controller.createToken({
                    ...body,
                    token: randomUUID(),
                    createdAt: dayjs(createdAt).unix()
                });
                
                return res.status(201).send(result);
            } catch (err) {
                console.log(err);
                
                return res.status(500).send({ message: "Error server" })
            }
    });

    api.get('/tokens/:token', 
        wrapBodyValidateSchema(headerSchema, "header"),  
        wrapBodyValidateSchema(paramsSchema, "params"),  
        async (req: Request, res: Response) => {
            try {
                const { token } = req.params;
                const autorization = req.headers.authorization as string;
                const client = await controller.getClient(autorization);

                if (client === null) {
                    return res.status(401).send({
                        "error": true,
                        "message": "Unauthorized"
                    });
                }

                const result = await controller.getToken(token);
                if (!result) {
                    return res.status(200).send({ 
                        message: "token expired" 
                    });
                }
                return res.status(200).send(result);
            
            } catch (err) {
                return res.status(500).send({ 
                    message: "Error server"
                });
            }
    });

    return api;
}