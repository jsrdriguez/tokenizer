import { AnyObjectSchema, ValidationError } from "yup";
import { Request, Response, NextFunction } from "express";

const validCrediCard = (value: string) => {
      if (/[^0-9-\s]+/.test(value)) return false;
  
      let nCheck = 0, bEven = false;
      value = value.replace(/\D/g, "");
  
      for (var n = value.length - 1; n >= 0; n--) {
          var cDigit = value.charAt(n),
                nDigit = parseInt(cDigit, 10);
  
          if (bEven && (nDigit *= 2) > 9) nDigit -= 9;
  
          nCheck += nDigit;
          bEven = !bEven;
      }
  
      return (nCheck % 10) == 0;
  }

const wrapBodyValidateSchema = (resourceSchema: AnyObjectSchema, type: string = "body") => 
    async (req:Request, res:Response, next: NextFunction) => {
    try {
        const reqTypeValidation:any = {
            "body": req.body,
            "header": req.headers,
            "params": req.params
        }

        await resourceSchema.validate(reqTypeValidation[type], { 
            recursive: true, 
            strict: true, 
            abortEarly: false
        });

        next();
    } catch (ex) {
        if (!(ex instanceof ValidationError)) {
            throw new Error("Not a validation error");
        }

        const errors: Record<string, string[]> = {};
        ex.inner.forEach((element) => {
            const path = element.path || "root";
            errors[path] ? errors[path].push(element.message) : (errors[path] = [element.message]);
        });

        return res.status(422).json({ 
            message:"Validation error",
            errors,
        });
    }
};
  
export {
    wrapBodyValidateSchema,
    validCrediCard
};