import dayjs from "dayjs";
import { object, string, number } from 'yup';
import { validCrediCard } from "./utils";

export interface BodyData {
    card_number: string,
    cvv: string,
    expiration_month: string,
    expiration_year: string,
    email: string
}

export const headerSchema = object({ 
    authorization: string()
        .required()
        .min(23)
        .max(31)
        .test('aut-card', 'authorization incorrect', (val) => {
            const formatValue = val.split(" ");
            if (formatValue[0] !== "Bearer") return false;
            if (formatValue[1].length < 16 || formatValue[1].length > 25) return false;

            return true
        })
});

export const paramsSchema = object({ 
    token: string()
        .required()
        .min(16)
        .max(16)
})

  
export const tokenSchema = object({
    card_number: number()
        .required()
        .test('len-card', 'card_number incorrect', (val) => {
            const parseValue = val.toString();
            if (parseValue.length < 13 || parseValue.length > 16) return false;
            
            return validCrediCard(parseValue);
        }),
    cvv: number()
        .required()
        .positive()
        .test('len', 'cvv incorrect', val => val.toString().length === 3 || val.toString().length === 4),
    expiration_month: string()
        .required()
        .min(1)
        .max(2)
        .matches(/^[0-9]+$/, "Must be only digits")
        .test('v-month', 'expiration_month is incorrect', (val) => {
            const validValue = parseInt(val);
            if (validValue >= 1 && validValue <= 12) {
                return true
            }
            return false
        }),
    expiration_year: string()
        .required()
        .max(4)
        .matches(/^[0-9]+$/, "Must be only digits")
        .test('v-year', 'expiration_year is incorrect', (val, context) => {
            const yearCurrent = dayjs().format('YYYY');
            const result = parseInt(val) - parseInt(yearCurrent);
            if(result < 0) return false;
            if (result === 0) {
                const monthCurrent = dayjs().format('MM');
                const resultMonth = parseInt(context.parent['expiration_month']) - parseInt(monthCurrent)
                if (resultMonth <= 0) return false
            }
            if (result >= 6) return false;
            return true
        }),
    email: string()
        .email()
        .required()
        .min(5)
        .max(100)
        .test('v-email', 'email is incorrect', (val) => {
            const validEmail = ["gmail.com", "hotmail.com", "yahoo.es"];
            const extEmail = val.split("@");

            return validEmail.includes(extEmail[1]);
        })
});

