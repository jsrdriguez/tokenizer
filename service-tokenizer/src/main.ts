import dotenv from "dotenv"
dotenv.config();

import serverExpressApp, { createExpressApp } from './infrastructure/server/express';

import DynamoClient from "./infrastructure/db/dynamo";
import DynamoTokensRepository from "./domain/dynamo-tokens-repository";

import sequalizeClient from "./infrastructure/db/sequalize";
import SequelizeClientRepository from "./domain/sequalize-client-repository";

import TokenController from "./app/token.controller";
import TokenRouter from "./app/routes";

const clientDynamo = DynamoClient();
const dynamoTokensRepository = new DynamoTokensRepository(clientDynamo);
const sequalizeClientRepository = new SequelizeClientRepository(sequalizeClient);

const tokenController = new TokenController(
    dynamoTokensRepository,
    sequalizeClientRepository
);

const routeToken = TokenRouter(tokenController);

const app = createExpressApp([
  routeToken
]);

serverExpressApp(app);

export default app;