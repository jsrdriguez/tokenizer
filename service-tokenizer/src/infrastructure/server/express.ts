import express, {Express, Response, Request, Router} from "express"
import config from "../../config";

export function createExpressApp(routers: Array<Router>) {
    const app:Express = express();

    app.use(express.json());
    app.use(express.urlencoded({ extended: false}));

    for (let router of routers) {
        app.use(router);
    }

    app.get("/", (req:Request, res: Response) => {
        return res.json({
            "version": "1.0.0",
            "app": "tokenizer",
        });
    });

    return app;
}

function server(app:Express) {
    const port = config.PORT;
    
    app.listen(port, () => {
        console.log(`Listening on port ${port}`);
    });
}

export default (app:Express) => server(app);
