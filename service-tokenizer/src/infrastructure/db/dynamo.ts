import { DynamoDB } from "@aws-sdk/client-dynamodb";
import config from "../../config";

function DynamoClient() {

    return new DynamoDB({ 
        region: config.AWS_REGION, 
        endpoint: config.AWS_DYNAMO_DB,
        credentials: {
            accessKeyId: config.AWS_ACCESS_KEY_ID,
            secretAccessKey: config.AWS_SECRET_ACCESS_KEY
        }
    }); 
}

export default DynamoClient