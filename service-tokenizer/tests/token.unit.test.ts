import 'jest'
import dayjs from 'dayjs';
import ShortUniqueId from 'short-unique-id';

import DynamoClient from '../src/infrastructure/db/dynamo';
import DynamoTokensRepository from '../src/domain/dynamo-tokens-repository';


describe('Unit tests', () => {
    const clientDynamo = DynamoClient();
    const dynamoTokensRepository = new DynamoTokensRepository(clientDynamo);

    it('Test save token', async () => {
        const { randomUUID } = new ShortUniqueId({ length: 16 });
        const createdAt = dayjs().format('YYYY-MM-DD HH:mm:ss');
        const token_id = randomUUID();
    
        const token = await dynamoTokensRepository.save({
            "token": token_id,
            "card_number": "4111111111111111",
            "cvv": "123",
            "expiration_month":"01",
            "expiration_year": "2023",
            "email": "test@test.com",
            "createdAt": dayjs(createdAt).unix()
        });

        expect({
            expiration_month: '01',
            expiration_year: '2023',
            token_id: token_id,
            card_number: '4111111111111111',
            email: 'test@test.com'
        }).toEqual(token);
    });

    it('Test get token', async () => {
        const { randomUUID } = new ShortUniqueId({ length: 16 });
        const createdAt = dayjs().format('YYYY-MM-DD HH:mm:ss');
        const token_id = randomUUID();
        
        await dynamoTokensRepository.save({
            "token": token_id,
            "card_number": "4111111111111111",
            "cvv": "123",
            "expiration_month":"01",
            "expiration_year": "2023",
            "email": "test@test.com",
            "createdAt": dayjs(createdAt).unix()
        });

        const tokenSearch = await dynamoTokensRepository.getCard(token_id);

        expect({
            expiration_month: '01',
            expiration_year: '2023',
            token_id: token_id,
            card_number: '4111111111111111',
            email: 'test@test.com'
        }).toEqual(tokenSearch);
    });
});