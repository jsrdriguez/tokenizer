import 'jest'
import request from 'supertest';


describe('Integration tests', () => {
    const bodyData = {
        "card_number": 4111111111111111,
        "cvv": 373,
        "expiration_month": "12",
        "expiration_year": "2025",
        "email": "isra@gmail.com"
    }
    
    it('Test save token', async() => {
        const response = await request('http://localhost:3000')
            .post("/tokens")
            .set('Accept', 'application/json')
            .set('Authorization', 'Bearer pk_test_UTCQSGcXW8bCyU59')
            .send(bodyData)
            
        expect(response.status).toBe(201);
    });

    it('Test get token', async () => {
        const response = await request('http://localhost:3000')
            .post("/tokens")
            .set('Accept', 'application/json')
            .set('Authorization', 'Bearer pk_test_UTCQSGcXW8bCyU59')
            .send(bodyData)
        
        const { token_id } = JSON.parse(response.text);

        const responseGet = await request('http://localhost:3000')
            .get("/tokens/"+token_id)
            .set('Accept', 'application/json')
            .set('Authorization', 'Bearer pk_test_UTCQSGcXW8bCyU59')

        
        expect(JSON.parse(responseGet.text)).toEqual({
            "card_number": 4111111111111111,
            "expiration_month": "12",
            "expiration_year": "2025",
            "email": "isra@gmail.com",
            "token_id": token_id
        });
    });

    it('Test validation save token', async() => {
        const response = await request('http://localhost:3000')
            .post("/tokens")
            .set('Accept', 'application/json')
            .set('Authorization', 'Bearer pk_test_UTCQSGcXW8bCyU59')
            .send({})
            
        expect(response.status).toBe(422);
    });

    it('Test validation header autorization', async() => {
        const response = await request('http://localhost:3000')
            .post("/tokens")
            .set('Accept', 'application/json')
            .set('Authorization', 'Bearer pk_test_UTCQSGcXW8bCyU9')
            .send(bodyData)
            
        expect(response.status).toBe(401);
    });

    it('Test validation header autorization', async() => {
        const responseGet = await request('http://localhost:3000')
            .get("/tokens/UTCQSGcXW8b")
            .set('Accept', 'application/json')
            .set('Authorization', 'Bearer pk_test_UTCQSGcXW8bCyU59')

        expect(responseGet.statusCode).toBe(422);
    });
});