Tokenización de Tarjetas

El siguiente test se realizo en un entorno de ubuntu/linux

## Pre Requisitos
    Tener instalado las siguientes herramientas:
    
    -   docker
    -   docker-compose 
    -   git


## Pasos para ejecutar el proyecto en local

1. Clonar el repositorio: 
```
git clone https://gitlab.com/jsrdriguez/tokenizer 
```

2. Construir el proyecto con el comando
```
docker-compose build
```

3. Iniciar el proyecto con el comando
```
docker-compose up -d
```

4. Crear la table en dynamoDB local
```
-   Ingresar a http://localhost:7000
-   Dar click al botón de "Create Table"
-   Nombre de la tabla: tokens
-   Especifica el "Hash Attribute Name": token_id

```
5. Rutas de la aplicación
```
En la raiz del projecto se encuentra un archivo postman: tokenizer.postman_local.json
con los requests para probar en local.

Para probar en el servicio de EKS se encuentra el archivo: tokenizerEKS.postman_eks.json

De no poder contar con postman y tener otra herramienta los requests en entorno local son: 

curl --location --request POST 'localhost:3000/tokens' \
--header 'Authorization: Bearer pk_test_UTCQSGcXW8bCyU59' \
--header 'Content-Type: application/json' \
--data-raw '{
    "card_number": 4111111111111111,
    "cvv": 373,
    "expiration_month": "12",
    "expiration_year": "2025",
    "email": "isra@gmail.com"
}'

curl --location --request GET 'localhost:3000/tokens/vHyv0HQ31hw5F39q' \
--header 'Authorization: Bearer pk_test_UTCQSGcXW8bCyU59'
```


5. Ejecución de pruebas en el servicio tokenizer
```
npm run test o npm run test:watch

Observación: Tener levantado los servicios para ejecutar con el comando:

docker-compose up -d
```

La ruta del proyecto en EKS es: <br/>
http://a1efc64ab5de34824948672742b99f70-a2e2f75bcc635e8e.elb.us-east-1.amazonaws.com
